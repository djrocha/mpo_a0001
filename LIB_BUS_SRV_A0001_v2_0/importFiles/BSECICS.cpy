      ****************************************************************
700238* 702678 11/06/17 OLSSA PROYECTO NUEVO CONTAC CENTER           *
      ****************************************************************
      *--------------------------------------------------------------*
      *  SISTEMA  : BSE - BUS DE SERVICIO EMPRESARIAL                *
      *  COPY     : BSECICS                                          *
      *  OPERACION: CONSULTA DE CLIENTES DE RM                       *
      *  LONGITUD : 2180 BYTES                                       *
      *--------------------------------------------------------------*
       01  REG-BSECICS.
      *--------------------------------------------------------------*
      *        * AREA DE DATOS DE ENTRADA                            *
      *--------------------------------------------------------------*
           02  BSECICS-INPUT.
      *        * TIPO DE DATO DE BUSQUEDA
      *            * 01 : CODIGO UNICO
      *            * 02 : NUMERO DE CUENTA
      *            * 03 : NUMERO DE TARJETA DE CREDITO
      *            * 04 : NUMERO DE TARJETA DE DEBITO
      *            * 05 : DOCUMENTO DE IDENTIDAD
               05  BSECICS-TIPO-BUSQUEDA             PIC  9(02).            000
               05  BSECICS-COD-UNICO                 PIC  9(10).            000
      *            * TIPO DE DOCUMENTO DE IDENTIDAD
      *            * 01 : DNI
      *            * 02 : RUC
      *            * 03 : CARNET DE EXTRANJERIA
      *            * 04 : CARNET DE IDENTIDAD
      *            * 05 : PASAPORTE
               05  BSECICS-DOC-IDENTIDAD             PIC  9(02).            000
               05  BSECICS-NRO-IDENTIDAD             PIC  X(11).            000
               05  BSECICS-CTA-CLIENTE.                                     000
                   10  BSECICS-CODIG-BANCO-CLTE      PIC  9(02).
                   10  BSECICS-CODIG-MONE-CLTE       PIC  9(03).
                   10  BSECICS-CODIG-CATE-CLTE       PIC  9(03).
                   10  BSECICS-NUMERO-CTA-CLTE.
                       15 BSECICS-COD-TIENDA-CLTE    PIC  9(03).
                       15 BSECICS-NRO-CUENTA-CLTE    PIC  9(10).
               05  BSECICS-TARJETA-CREDITO-DEBITO    PIC  X(19).            000
               05  BSECICS-FILLER-INPUT              PIC  X(101).           000
      *--------------------------------------------------------------*
      *        * AREA DE DATOS DE SALIDA                             *
      *--------------------------------------------------------------*
           02  BSECICS-OUTPUT.
      *--------------------------------------------------------------*
               05  BSECICS-COD-RESPUESTA             PIC X(02).
               05  BSECICS-DES-RESPUESTA             PIC X(60).
               05  BSECICS-DES-SEGMENTACION          PIC X(15).
      *--------------------------------------------------------------*
      *        * AREA DE DATOS DE SALIDA                             *
      *--------------------------------------------------------------*
           02  BSECICS-RETORNO-CLIENTE.
                   04  BSECICS-DATOS-CLIENTE.
                       05  BSECICS-CUST-CTLS-O.
                           10  BSECICS-CTL1-CUST-O    PIC X(04).
                           10  BSECICS-CTL2-CUST-O    PIC X(04).
                           10  BSECICS-CTL3-CUST-O    PIC X(04).
                           10  BSECICS-CTL4-CUST-O    PIC X(04).
      *        NUMERO DEL CODIGO UNICO TITULAR DE LA CUENTA
                       05  BSECICS-CUST-NBR-O         PIC X(14).
      *
      *--------INFORMACION EXTRAIDA DEL SEGMENTO RAIZ DE CLIENTES----*
      *
                   04  BSECICS-SEGMENTO-RAIZ.
084084*        TIPO DE PERSONA
                       05  BSECICS-TIPO-PERSONA       PIC X(01).
085124*        NOMBRE DEL CLIENTE
                       05  BSECICS-NOMBRE-CLIENTE     PIC X(40).
125125*        TIPO DOCUMENTO IDENTIDAD
                       05  BSECICS-TIPO-DOCU          PIC X(01).
126136*        NUMERO DOCUMENTO IDENTIDAD
                       05  BSECICS-NRO-DOCU           PIC X(11).
137149*        NOMBRE CORTO DEL CLIENTE
                       05  BSECICS-SHORT-NAME         PIC X(13).
150209*        DIRECCION DEL CLIENTE
                       05  BSECICS-DIRECCION-CLTE     PIC X(60).
210214*        CODIGO DE SECTORISTA
                       05  BSECICS-COD-SECTORISTA     PIC X(05).
215216*        SITUACION DEL CLIENTE
                       05  BSECICS-SITU-CLIENTE       PIC X(02).
217226*        TIPO DE BANCA
FO5238                 05  BSECICS-TIPO-BANCA         PIC X(01).
104231*        INDICADOR SEGMENTADO IMAGINE
104231                 05  BSECICS-ID-SEGM-IMG        PIC X(02).
FO5238*        ESPACIO DISPONIBLE
104231                 05  BSECICS-FILL-USR-1         PIC X(07).
227238*        CODIGO SBYS
                       05  BSECICS-CODIGO-SBYS        PIC X(12).
239242*        A�O CALIFICACION SBYS
                       05  BSECICS-YEAR-SBYS-SSAA     PIC X(04).
243244*        PERIODO CALIFICACION SBYS
                       05  BSECICS-MES-SBYS-MM        PIC X(02).
245246*        CODIGO DE RETORNO ARCHIVO CERRADO
                       05  BSECICS-RETURN-CODE-NOTOPEN      PIC X(02).
247249*        CODIGO CALIFICACION IB
                       05  BSECICS-CALIFICACION-IBK   PIC X(03).
250253*        A�O CALIFICACION IB
                       05  BSECICS-YEAR-CALIF-IBK     PIC X(04).
254255*        PERIODO CALIFICACION IB
                       05  BSECICS-MES-CALIF-IBK      PIC X(02).
256256*        CODIGO CALIFICACION PAGO CHEQUE
                       05  BSECICS-CALIF-PAGO-CHEQ    PIC X(01).
257261*        CODIGO OFICIAL MARKETING ASIGNADO
                       05  BSECICS-MKTG-OFF-CD        PIC X(05).
262266*        CODIGO OFICIAL PRESTAMOS ASIGNADO
                       05  BSECICS-LN-OFF-CD          PIC X(05).
267271*        CODIGO OFICIAL DEPOSITOS ASIGNADO
                       05  BSECICS-DEP-OFF-CD         PIC X(05).
272276*        OFICINA APERTURA CLIENTE ( USO FUTURO )
                       05  BSECICS-OPENING-BR         PIC X(05).
277284*        FECHA ULTIMA MODIFICACION
                       05  BSECICS-LST-MAINT-DT       PIC X(08).
285292*        USUARIO REALIZO ULTIMA MODIFICACION
                       05  BSECICS-LST-MAINT-EMP-ID   PIC X(08).
283293*        INDICADOR SI ES EMPLEADO
                       05  BSECICS-IND-EMPLEADO       PIC X(01).
294294*        CALIFICACION SBYS
                       05  BSECICS-TIN-CERT-CD        PIC X(01).
295295*        TIPO DE BANCA SBYS
                       05  BSECICS-TIPO-BANCA-SBYS    PIC X(01).
296297*        INDICADOR SOLICITUD DE REFRENDO
                       05  BSECICS-TX-CTRY-CD         PIC X(02).
296297*        FECHA INGRESO DEL CLIENTE
                       05  BSECICS-FEC-INGRESO-CLTE   PIC X(08).
      *---------------------------------------------------- 214 -----*
298320*        INFORMACION EXTRAIDA DEL SEGMENTO TELEFONO DE CLIENTES
                   04  BSECICS-SEGMENTO-TELEFONO.
298298*        TIPO TELEFONO (R=RESIDENC. C=CELULAR  F=FAX  N=NEGOCIO)
                       05  BSECICS-TIPO-TELEFONO      PIC X(01).
299316*        NUMERO DE TELEFONO
                       05  BSECICS-NRO-TELEFONO       PIC X(18).
317320*        NUMERO DE EXTENSION O ANEXO
                       05  BSECICS-ANEXO-EXT          PIC X(04).
      *----------------------------------------------------- 23 -----*
321386*        INFORMACION EXTRAIDA DEL SEGMENTO PERSONAL DE CLIENTES
                   04  BSECICS-PERSONAL-SEGMENT.
321325*        NACIONALIDAD
                       05  BSECICS-NACIONALIDAD       PIC X(01).
326333*        FECHA DE NACIMIENTO O CONSTITUCION
                       05  BSECICS-FECHA-NACIMIENTO   PIC X(08).
334334*        SEXO
                       05  BSECICS-SEXO               PIC X(01).
335335*        ESTADO CIVIL
                       05  BSECICS-ESTADO-CIVIL       PIC X(01).
336337*        NIVEL DE EDUCACION
                       05  BSECICS-NIVEL-EDUCACION    PIC X(02).
338377*        NOMBRE CONYUGE
                       05  BSECICS-NOMBRE-CONYUGE     PIC X(40).
378386*        DOCUMENTO CONYUGE
                       05  BSECICS-DOC-CONYUGE        PIC X(09).
      *----------------------------------------------------- 62 -----*
387559*        INFORMACION EXTRAIDA DEL SEGMENTO EMPLEADOR DE CLIENTES
                   04  BSECICS-EMPLOYMENT-SEGMENT.
387394*        FECHA INGRESO A LA EMPRESA
                       05  BSECICS-EMPL-START-DT      PIC X(08).
395402*        FECHA TERMINO VINCULO CON LA EMPRESA
                       05  BSECICS-EMPL-STP-DT        PIC X(08).
403442*        NOMBRE DE LA EMPRESA
                       05  BSECICS-EMPR-NAME          PIC X(40).
443482*        DIRECCION DE LA EMPRESA
                       05  BSECICS-EMPR-ADDR          PIC X(40).
483522*        DPTO/PROVINCIA/DISTRITO DE DIRECCION DE LA EMPRESA
                       05  BSECICS-EMPR-CITY-ST-ZIP   PIC X(40).
523540*        TELEFONO DE LA EMPRESA
                       05  BSECICS-EMPR-PH            PIC X(18).
541544*        ANEXO O EXTENSION DEL TELEFONO DE LA EMPRESA
                       05  BSECICS-EMPR-PH-EXT        PIC X(04).
545559*        CARGO QUE DESEMPE�A EN LA EMPRESA
                       05  BSECICS-JOB-TITL           PIC X(15).
      *---------------------------------------------------- 173 -----*
560663*        INFORMACION EXTRAIDA DEL SEGMENTO COMERCIAL DE CLIENTES
                   04  BSECICS-COMERCIAL-SEGMENT.
560563*        CODIGO CIIU
                       05  BSECICS-CODIGO-CIIU        PIC X(04).
564567*        CODIGO GRUPO ECONOMICO
                       05  BSECICS-GRUPO-ECONOMICO    PIC X(04).
568607*        ACTIVIDAD PRINCIPAL ( SOLO COMERCIALES )
                       05  BSECICS-BUS-DESC           PIC X(40).
608632*        NOMBRE COMERCIAL ( SOLO COMERCIALES )
                       05  BSECICS-COMM-NAME          PIC X(25).
633652*        SIGLAS           ( SOLO COMERCIALES )
                       05  BSECICS-ACRONYMS           PIC X(20).
653653*        INDICADOR FIN DE LUCRO
                       05  BSECICS-LUC-CUST-LOGIC-IND PIC X(01).
654663*        TIPO DE SOCIEDAD
                       05  BSECICS-TIPO-DE-SOCIEDAD   PIC X(10).
664664*        MAGNITUD DE NEGOCIO
                       05  BSECICS-SMALL-BUS-IND      PIC X(01).
      *----------------------------------------------------- 115 ----*
665734*        INFORMACION EXTRAIDA DEL SEGMENTO DIRECCION DE CLIENTES
                   04  BSECICS-ADDRESS-MS-SEGMENT.
665684*        DEPARTAMENTO
                       05  BSECICS-DEPARTMENT         PIC X(20).
685704*        PROVINCIA
                       05  BSECICS-ST                 PIC X(20).
705724*        DISTRITO
                       05  BSECICS-CITY               PIC X(20).
729738*        CODIGO POSTAL
                       05  BSECICS-ZIP-CD             PIC X(10).
739798*        REFERENCIA DIRECCION
                       05  BSECICS-REFERENCIA         PIC X(60).
      *----------------------------------------------------- 130 ----*
799878*        NOMBRE LARGO DEL CLIENTE
                   04  BSECICS-LONG-NAME              PIC X(80).
                   04  FILLER REDEFINES BSECICS-LONG-NAME.
                       05  BSECICS-PATERNAL-NAME      PIC X(20).
                       05  BSECICS-MATERNAL-NAME      PIC X(20).
                       05  BSECICS-FIRST-NAME         PIC X(20).
                       05  BSECICS-SECOND-NAME        PIC X(20).
879880*        CODIGO DE SUBTIPO DE CLIENTE
                   04  BSECICS-CUST-SUB-TYP-CD        PIC X(02).
881906*        CODIGO DE KEY NAME X NOMBRE
                   04  BSECICS-XREF-NAME              PIC X(26).
907907*        FLAG CHEQUES RECHAZADOS (CON-SELLO)
                   04  BSECICS-FLAG-CHEQ-RECH         PIC X(01).
908915*        FECHA ULTIMO CHEQUE RECHAZADO (CON-SELLO)
                   04  BSECICS-DT-CHEQ-RECH           PIC X(08).
916917*        FLAG DE CLIENTE EXCEPTUADO
                   04  BSECICS-FLAG-CLIE-EXCEP        PIC X(02).
      *--------------------------------------------------------------*
918939*        TELEFONO C.TRABAJO (DE SEGM.TELEFONOS PH-TYP-CD='T')
                   04  BSECICS-PHONE-CT.
918935*        NUMERO DE TELEFONO C.TRABAJO
                       05  BSECICS-PH-NBR-CT          PIC X(18).
936939*        NUMERO DE EXTENSION O ANEXO
                       05  BSECICS-PH-EXT-CT          PIC X(04).
      *--------------------------------------------------------------*
940959*        PAIS  DEL BANQUERO (DIRECCION: IB RETENER...)
                   04  BSECICS-CTRY                   PIC X(20).
960979*        PLAZA DEL BANQUERO (LINEA NOMBRES C/TIPO = 'D')
                   04  BSECICS-PLAZA-BQ               PIC X(20).
      *--------------------------------------------------------------*
980981*        FLAG CHEQUES RECHAZADOS (SIN-SELLO)
                   04  BSECICS-FLAG-CHEQ-RECH-SS      PIC X(02).
982989*        FECHA ULTIMO CHEQUE RECHAZADO (SIN-SELLO)
                   04  BSECICS-DT-CHEQ-RECH-SS        PIC X(08).
      *--------------------------------------------------------------*
990990*        TIPO CAMBIO PREFERENCIAL (RMIB130)
                   04  BSECICS-TI-CAMB-PREF           PIC X(02).
      *--------------------------------------------------------------*
992992*        FLAG SOLICITA ACTUALIZAR DATA EN RM   ("S"-SI, "N"-NO)
                   04  BSECICS-IL-SOLI-UPDT           PIC X(01).
      *--------------------------------------------------------------*
993996*        CODIGO PAIS DE RESIDENCIA (DEFAULT="4028"-PERU)
                   04  BSECICS-PAIS                   PIC X(04).
      *--------------------------------------------------------------*
997000*        FECHA ACTUALIZACION
      *            04  BSECICS-DT-LST-CUST-CONTACT    PIC 9(8).
				   04  BSECICS-DT-LST-CUST-CONTACT    PIC X(8).
      *
      *-------------------SEGMENTO TELEFONO DE CLIENTES--------------*
      *
                   04  BSECICS-SEGMENTO-TELEFONO-CLI.
      *        TIPO TELEFONO C=CELULAR
                       05  BSECICS-PH-TYP-CL          PIC X(01).
      *        NUMERO DE TELEFONO
                       05  BSECICS-PH-NBR-CL          PIC X(18).
      *        TIPO TELEFONO F=FAX
                       05  BSECICS-PH-TYP-FX          PIC X(01).
      *        NUMERO DE TELEFONO
                       05  BSECICS-PH-NBR-FX          PIC X(18).
      *        TIPO TELEFONO N=NEGOCIO
                       05  BSECICS-PH-TYP-RS          PIC X(01).
      *        NUMERO DE TELEFONO
                       05  BSECICS-PH-NBR-RS          PIC X(18).
      *        NUMERO DE EXTENSION O ANEXO
                       05  BSECICS-PH-EXT-RS          PIC X(04).
      *        TIPO TELEFONO N=NEGOCIO
                       05  BSECICS-PH-TYP-NG          PIC X(01).
      *        NUMERO DE TELEFONO
                       05  BSECICS-PH-NBR-NG          PIC X(18).
      *        NUMERO DE EXTENSION O ANEXO
                       05  BSECICS-PH-EXT-NG          PIC X(04).
      *
      *----------------INFORMACION VARIAS   -------------------------*
      *
                   04  BSECICS-SEGMENTO-VARIOS.
      *        CORREO ELECTRONICO PERSONAL
                       05  BSECICS-E-MAIL-PER         PIC X(30).
      *        TIPO DE VIVIENDA L=CASA DE FAM O=PROPIET R=ALQUILAD
                       05  BSECICS-OWN-RENT-CD        PIC X(01).
      *        TIPO DE TRABAJADOR N=DEPENDIENTE Y=INDEPENDIENTE
                       05  BSECICS-SELF-EMP-IND       PIC X(01).
      *        CORREO ELECTRONICO DE LA EMPRESA
                       05  BSECICS-E-MAIL-TRA         PIC X(30).
      *        TIPO DE DOCUMENTO DEL CONYUGE
                       05  BSECICS-TIN-CD-CONYG       PIC X(01).
      *        FECHA DE NAC. DEL CONYUGE (NO EXISTE EN RM)
                       05  BSECICS-SPOUSE-DOB         PIC X(08).
      *        TIPO DE CAMBIO PREFERENCIAL
                       05  BSECICS-TI-CAMB-PREF-VA    PIC X(02).
      *        DIRECCION DEL CLIENTE (RMADR LINEA-2 CHAR-40)
                       05  BSECICS-REFER-ADDR         PIC X(60).
      *        DIRECCION DEL CLIENTE (RMADR LINEA-3 CHAR-40)
                       05  BSECICS-REFER-ADDR2        PIC X(60).
      *        DIRECCION DISTRITO
                       05  BSECICS-CITY               PIC X(20).
      *        DIRECCION PROVINCIA
                       05  BSECICS-ST                 PIC X(20).
      *        DIRECCION DEPARTAMENTO
                       05  BSECICS-IB-DEPT            PIC X(20).
      *        FECHA DE ULTIMO CONTACTO CON EL CLIENTE
                       05  BSECICS-FE-LST-CUST-CONTACT PIC X(08).
      *--------------------------------------------------------------*
      *        PAIS DE ORIGEN
                   04  BSECICS-PASSPORT-CTRY          PIC X(04).
      *
      *--------ANEXO (5 DIGITOS) R=RESIDENCIA/N=NEGOCIO/T=TRABAJO ---*
      *
                   04  BSECICS-ANEXOS-5DIGITOS.
                       05  BSECICS-PH-EXT-RS5         PIC X(05).
                       05  BSECICS-PH-EXT-NG5         PIC X(05).
                       05  BSECICS-PH-EXT-CT5         PIC X(05).
      *
      *--------EMAIL PERSONAL/TRABAJO DE 60 CARACTERES --------------*
      *
                   04  BSECICS-RMEMLC-EMAILS.
                       05 BSECICS-EMAIL-PER-60         PIC X(60).
                       05 BSECICS-EMAIL-TRA-60         PIC X(60).
      *
      *--------CODIGO UBIGEO PERSONA     ----------------------------*
      *
                   04  BSECICS-COD-UBIGEO              PIC X(06).
      *--------------------------------------------------------------*
      *--------------------------------------------------------------*
      *        ESPACIO DISPONIBLE
                   04  BSECICS-FILLER-OUTPUT           PIC X(670).
      *--------------------------------------------------------------*
